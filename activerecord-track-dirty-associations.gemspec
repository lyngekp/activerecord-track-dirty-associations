lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "version"

Gem::Specification.new do |spec|
  spec.name          = "activerecord-track-dirty-associations"
  spec.version       = Activerecord::TrackDirtyAssociations::VERSION
  spec.authors       = ["Lynge Poulsgaard"]
  spec.email         = ["lpo@webshipper.com"]

  spec.summary       = %q{Define track_dirty_associations method}
  spec.description   = %q{This gem enables tracking of dirty associations on ActiveRecord objects}
  spec.homepage      = %q{https://webshipper.io}
  spec.license       = "MIT"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/lyngekp/activerecord-track-dirty-associations"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_runtime_dependency 'activerecord', '~> 5.0'
end
