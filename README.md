# README #

This is the readme for the activerecord-track-dirty-associations gem. A gem that adds the functionality to track dirty associations in activerecords.

### How do I get set up? ###

* Add the gem to your gemfile
* Add the line track_dirty_associations *association_types to the model you wish to be able to track dirty associations for
* You can now call dirty_associations? to check if tracked associations have changed, be aware that tracking an association does not automatically track that models associations. For this to work you have to add track_dirty_associations for the associations you want to track on that model as well.
* You can use dirty_associations to get all dirty associations and their changes (added, removed or changed)
* You can use {association}_changed? to see if a specific association have changed or {association}_changes to get the changes for a specific association.